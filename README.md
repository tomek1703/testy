This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Firt of all You need to install it using npm i.
This app was create to learn basics of testing in React using Jest. 
You can star this app using npm start
You can test it using command npm test.


Below you will find some information on how to perform common tasks.<br>
You can find the most recent version of this guide [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).
